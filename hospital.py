import random 
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter

arr = []
number_of_days = 31 * 12 * 20

counter = 0

for _ in range(number_of_days):
    day = 0
    for _ in range(24):
        for _ in range(1):
            day += random.expovariate(300)*3600
    arr.append(day)

    if day >= 369: 
        counter+=1

print(f"Days with at least 369 arrivals: {counter}") 

fig, ax1 = plt.subplots()
fig.suptitle("Hospital arrival simulation")

ax1.set_xlabel("Arrivals")
ax1.set_ylabel("Probability")

plt.hist(arr, bins=20, density=True)
plt.gca().yaxis.set_major_formatter(PercentFormatter(1))

fig.tight_layout()
fig.savefig("hospital.png")
plt.show()
