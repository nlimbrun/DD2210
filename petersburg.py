import random
import matplotlib.pyplot as plt

boxes_n = []
boxes_prices = []

n_boxes = 20

for _ in range(n_boxes):
    n_times = 100
    n_arr, n_price = [], []

    for i in range(n_times):
        n = 0
        tails = bool(random.randint(0, 1))
        while tails and n < 23:
            tails = bool(random.randint(0, 1))
            n += 1
        n_arr.append(n)
        n_price.append(2 ** n)

    avg_n = sum(n_arr) / n_times
    avg_price = sum(n_price) / n_times
    
    boxes_n.append(avg_n)
    boxes_prices.append(avg_price)

    # print(f"Avervate tails in a row {avg_n}")
    # print(f"Average prize {avg_price} SEK")

plot = True
if plot:
    fig, ax1 = plt.subplots()
    fig.suptitle("St Petersburg Paradox Simulation")

    color = "tab:red"
    ax1.set_xlabel("toss")
    ax1.set_ylabel("tails in a row", color=color)
    ax1.bar(range(n_boxes), boxes_n, color=color)
    # ax1.plot(range(n_boxes), [avg_n for _ in range(n_boxes)], "--", color=color)
    ax1.tick_params(axis="y", labelcolor=color)

    ax2 = ax1.twinx()
    color = "tab:blue"
    ax2.set_ylabel("price", color=color)
    # ax2.set_yscale('log',base=10)
    ax2.bar(range(n_boxes), boxes_prices, color=color)
    # ax2.plot(range(n_boxes), [avg_price for _ in range(n_boxes)], "--", color=color)
    ax2.tick_params(axis="y", labelcolor=color)

    fig.tight_layout()
    fig.savefig("peterburg.png")
    plt.show()
